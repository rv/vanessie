---
title: "My Violent Heart"
date: "2009-11-04"
available: false
---

Skull doll

2009

22 cm / 8.7 inch

Agapornis skull, apoxie, miniature bricks and tiles, flints/pebbles, leather, jersey, transparent sheet with song text from Nine Inch Nails, wooden base.
