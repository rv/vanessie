---
title: "Pleasure"
date: "2010-03-12"
available: false
---

Skull doll

2010

20 cm / 7.9 inch

Pleasure is made from a real bird skull and her body is made of Aves apoxie over a stainless steel armature. \
She has got high quality English viscose hair. \
Her clothes, cat o'nine tails and base are made of very soft lambs leather. \
Pleasure's bondage is in Shibari (Japanese bondage art) style.
