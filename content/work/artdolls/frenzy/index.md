---
title: "Frenzy"
date: "2011-03-31"
available: false
---

Skull doll

2011

58 cm / 22.8 inch (incl her double base)

Here is member four from the Psychobilly gang: Flaming Frenzy. \
Frenzy is made of apoxie and paperclay over a metal wire and rod/tube frame. \
Her gorgeous head is an artificial museum quality cat skull from BoneClones. \
She is painted with acrylics and has realistic eyes with long black eyelashes for that flirty look. \
Her hair is made from red painted locks from Gotland lambs. \
Frenzy's dress and her highschool sweetheart baseball jacket are made from jersey. \
Her petticoat is from black tule. \
She also wears nylons, roll socks and sneakers.
