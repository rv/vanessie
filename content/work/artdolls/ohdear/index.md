---
title: "Oh Dear...!"
date: "2009-10-01"
available: false
---

Wallmounted skull artpiece

2009

The skull's size is 20 cm x 10 cm (7.9 inch x 3.9 inch). \
The shield size is 40 cm (15.7 inch).

She's made of a real pig skull with German mouthblown glass eyes. \
Her hair is from A+ quality English viscose with pearl hair and hat pins. \
The hairpiece is from lace, chicken and pheasant feathers, satin roses, antique gold guilded hairpin with a real garnet and a real rabbit skull. \
She's wearing an antique Edwardian lace bodice with a silk pleated collar and a cameo brooch with fresh water pearls. \
She is mounted on a German wooden swine/deer shield.
