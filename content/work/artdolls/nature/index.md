---
title: "Mother Nature"
date: "2012-05-20"
available: false
---

Skull dolls

2012

H x W x D (approximately):
- 48 x 20 x 20 cm<
= 18.9 x 7.9 x 7.9 inch

Massive granite base. \
Both bodies are made from Paperclay and apoxie over a metal frame, painted and sealed with acrylics and varnish. \
Their heads are owl skulls sculpted with Paperclay, handmade glass eyes, and Mother Nature has got lashes. \
Both got high quality English viscose hair adorned with pink silk ribbons. \
Mother Nature's dress, trousers, and hat are made from dust grey linnen and are hand embroidered by me with DMC embrodery silk. \
The hat is also adorned with vines from three different colors seed beads, and 2 very detailed paper butterflies. \
She wears olive green lambsleather boots with metal buckles. \
The little girl is also dressed in dust grey linnen adorned with silk embroidered flowers and silk ribbon. \
Her little Mary Jane's are made from olive green lambs leather with an Swarovski crystal button (just 1.3 mm big!). \
The little one is pulling a wooden cart filled with sand and a wooden Japanese butterfly cage on top. \
The cage is filled with paper butterflies.
