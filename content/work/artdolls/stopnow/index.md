---
title: "Stop Now!"
date: "2012-09-18"
available: false
---

Skull doll

2012

H x W x D approx.
- 22 x 20 x 25 cm
- 8.7 x 7.9 x 9.8 inch

Stop Now her skull is a real Kingfisher bird skull with glass eyes and high quality English viscose hair. \
Her body is sculpted with paperclay over a strong metal armature, painted with acrylics and sealed with artist gel varnish. \
Stop Now is dressed with antique Victorian lace, silk and tulle adorned with Swarovski crystals and fresh water pearls. \
She's wearing a tiara with vintage crystals and fresh water pearls. \
Her base is a Belgium fossil stone on which she is permanently attached.
