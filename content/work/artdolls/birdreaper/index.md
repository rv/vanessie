---
title: "The Birdreaper"
date: "2009-10-01"
available: false
---

Skull doll

48 cm / 18.9 inch

Bird skulls, Apoxie, polymer, taft silk, lace, feathers, cinnamon cane, oxbone skull beads, mohair flocking, wooden base, digital photoframe \
Extra feature: a digital photoframe, manual  and usb cable.

