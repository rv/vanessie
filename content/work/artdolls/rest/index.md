---
title: "Rest My Little One"
date: "2012-09-12"
available: false
---

Memento Mori wall box

2012

H x W x D approx.
- 25 x 21 x 4 cm
- 9.8 x 9.4 x 1.6 inch

Little One her skull is the real skull of a stillborn premature kitten that was found by animal welfare workers, together with her stillborn siblings, in the home of a cat lady. \
Her body is made from paperclay, painted with acrylics and sealed with artist gel varnish. \
The clothes of Little One are made from antique fabric and antique laces adorned with silk bows and fresh water sead pearls. \
Little One is holding a kids rosary made from antique African trading beads and a small metal cross. \
She is mounted on antique burgundy velvet in an antique relique frame adorned with many burgundy paper, dried, velvet and antique wax flowers. \
The glass of the frame is domed.
