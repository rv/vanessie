---
title: "Yulia"
date: "2012-12-20"
available: false
---

Skull bust

2012

H x W x D approx.
- 30 x 20 x 20 cm
- 11.8 x 7.8 x 7.8 inch

Yulia is completely sculpted from paperclay (including the skull), painted with acrylics/pastels and sealed with Mr. Clear. \
Her kokoshnik hair is made from high quality English viscose and adorned with Swarovski crystal beads and a vintage pearl. \
She wears highly detailed sculpted clothes finished with fresh water pearls. \
She is attached to a metal base, with a long stainless steel rod going all through the bust, and felt underneath to protect your furniture.

