---
title: "Stop Staring!"
date: "2012-09-10"
available: false
---

Wall mount

2012

H x W x D approx
- 58 x 48 x 20 cm
- 22.8 x 18.9 x 7.9 inch

Her head is an English Bulldog half skull with deer antlers. \
The eyes are German mouth blown glass eyes and she's wearing false lashes. \
She's wearing a wig made of lots of high quality English viscose hair adorned with paper roses, dried roses, velvet roses and fake berries. \
Her nose is made from lambs leather. \
The frame is an antique frame from around 1800 (with a new back) and has got multiple flaws due to it's age but that only adds to it charms. \
The frame is upholstered with bordeaux velvet and finished with black velvet piping.
