---
title: "Caged Beauty #1 Alaia"
date: "2013-12-15"
available: false
---
- height 46 cm / 18 inch
- diameter 22 cm / 8.6 inch
 
Alaia her head is a sculpted paperclay skull head with a black hare mask. \
The back of her ears are gilded with real 23.75 karat gold. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers acrylics, and each layer also sealed with Mr Clear. \
Alaia is wearing black silk velvet hotpants with silk ribbons, an organza plisse silk dress, and a silk corset with hand embroidered tralis and hand beaded with many gold glass seed beads over an antique brocade panel. \
Her legs are painted black with black silk and golden bows. \
Alaia is also wearing a cage crinoline made of black aluminium with silk bows and golden crimpbeads. \
She has a very long ponytail made from a human hair extension. \
Her slender skeleton hands are finished with the top of her fingers gilded (with real 23.75 karat gold). \
She is posed on a hand turned wooden base in black satin finish.

{{< figure src="photo.jpg" title="Alaia" >}}
