---
title: "Olly and Olivia - Conjoined Twins"
date: "2012-09-30"
available: false
---

Wall mount

2012

H x W x D approx.
- 30 x 35 x 5 cm
- 11.8 x 13.7 x 1.9 inch

The conjoined skull of Olly and Olivia is made from two different real cat skulls and joined with apoxie. \
Her eyes are two different types of realistic BJD eyes and she wears fake eyelashes for humans. \
I used high quality English viscose for her hair and adorned it with two black silk bows. \
She wears a collar from antique Victorian lace with a beautiful glass focal bead. \
The skull is attached on a black velvet background in a gorgeous antique gesso frame.
