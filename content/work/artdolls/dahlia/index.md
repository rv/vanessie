---
title: "Dahlia Noir"
date: "2011-12-04"
available: false
---

Skull doll

2011

- 46 cm / 18 inches without base
- 50 cm / 19.7 inches with base

Owl skull and body sculpted by me from paperclay (over a metal rod and tube armature), painted with acrylics, handmade glass eyes and black viscose hair. \
Her hat is made from felt with a burgundy silk ribbon and bow. \
She wears a fresh water pearl necklace and a cameo. \
Her clothes are made from silk, velvet and Edwardian lace finished with black Swarovski crystals. \
Her base is massive black granite with metal feet.
