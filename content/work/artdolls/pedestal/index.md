---
title: "High Upon A Pedestal"
date: "2010-03-24"
available: false
---

Skull doll

2010

- 16 x 16 x 18 cm (WxBxH)
- 6.3 x 6.3 x 7.1 inch (WxBxH)

One of the 7 deadly sins: Pride/Vanity. \
Her torso is placed on a pedestal (like all pride/vain people want) and a picture of herself is in the frame (vain people can't get enough of themself).

This piece is made in the Rococo style (Rococo is a style of 18th century French art and interior design). \
The torso is made from a real bird skull and apoxie sculpt. \
Her hair is high quality English viscose and is highly decorated, in the late Rococo style, with sails, roses, leaves, feathers, and pearls. \
Her jacket is made from Thai brocade and white lace. \
The base is also handmade and decorated with altered miniture ornaments and apoxie sculpt. \
The back of the base is decorated with gold leaf.
