---
title: "Kitsune"
date: "2015-07-13"
available: false
---

2015

H x W x D approx
- 58 x 34 x 27 cm
- 22.8 x 13.3 x 10.6 inch

Kitsune is sculpted from paperclay in many layers over a handmade tinfoil armature which was removed later to create her hollow body. \
She is painted with high quality acrylics and sealed with many layers of Mr. Superclear. \
Her ponytail is made from many 60 cm/23 inch long human hair extensions. \
She is dressed in a handmade black couture quality flax top with a black flax corset that is hand embroidered with many 1.2 mm/0.04 inch seed beads. \
Kitsune has a diorama in her body that features a forest scene where the forest animals take over the control from the hunters. \
This diorama is protected against dust by an antique domed glass and the antique golden frame from this glass is used in her corset. \
There are 18 bright white LEDs inside her body to show the diorama. \
An international universal power adaptor is supplied.

Kitsune is a very large (the size of a young kid) and very heavy bust, she weights 4.7 kg/10.36 lbs.

As with all my work: she is completely handmade so there are irregularities, flaws, and imperfections. \
For me, and my many collectors, this doesn't subtract anything from her overall beauty but if you go for perfection she isn't your doll.
