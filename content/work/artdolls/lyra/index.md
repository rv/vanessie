---
title: "Lyra"
date: "2012-07-19"
available: false
---

Skull doll

2012

- Bust 29 cm / 11.4 inch
- Total piece 54 cm / 21.3 inch

The skull is a real house cat skull with realistic acrylic BJD eyes and lashes. \
Her hair and lyre are made from high quality black English viscose adorned with Swarovski crystals, silk embroidery thread strings, and clay flowers. \
Her torso is sculpted with paperclay over a messing rod frame. \
The bust is dressed with pink dupioni silk and black antique lace from the Victorian era, finished with tiny Swarovski crystals and a black silk bow. \
She is posed (removable for safe shipping) on an antique tin church candleholder adorned with a pink silk bow and Swarovski crystal drop.
