---
title: "Ikebana"
date: "2017-03-04"
available: false
---

2017

The Japanese art of flower arrangement, also known as kadō. \
Ikebana reached its first zenith in the 16th century under the influence of Buddhist teamasters and has grown over the centuries.

H x W approx.
- 61 x 32 cm
- 24 x 12.6 inch

Ikebana's head is a resin Vervet monkey skull (from a mold made of a real Vervet monkey skull) painted high gloss black with a black ponytail from a human hair extension. \
She has an Ikebana arrangement with a resin poppy and real goldleaf (23 carat) in her skull. \
Her body is sculpted from paper clay and painted black. \
Ikebana her dress, shaped in the form of a vase (the vessel for an Ikebana arrangement), is made of dark grey and black felt with a machine embroidery (made by me) of poppies on the front and tiny red buttons on the back. \
She wears black tights and is posed on a black base.
