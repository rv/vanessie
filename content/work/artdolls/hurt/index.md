---
title: "Hurt"
date: "2012-10-24"
available: false
---

Skull doll

2012

H x W x D approx.
- 20 x 20 x 20 cm
- 7.9 x 7.9 x 7.9 inch

Hurt her skull is a real bird skull with closed eyes and lashes. \
Her hair is made from high quality black English viscose adorned with vintage Swarovski crystals. \
Her body is sculpted with paperclay over a metal frame. \
Hurt is dressed in plum vintage embroidered silk with seed beads, gold sequins, silk bows, and many layers of plum tule finished with tiny Swarovski crystal beads. \
She is posed (permanently) on a heavy granite base.
