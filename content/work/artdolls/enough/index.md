---
title: "Enough!"
date: "2012-08-09"
available: false
---

Skull doll

2012

H x W x D approximately
- 20 x 20 x 20 cm<
- 7.9 x 7.9 x 7.9 inch

Enough! her skull is a real Bearded Barbet skull with glass eyes and lashes. \
Her hair is made from high quality black English viscose adorned with antique gold gilded ornaments with a red garnet. \
Her body is sculpted with paperclay over a metal frame. \
Enough! is dressed in berry red vintage embroidered silk and many layers of beryy red tule finished with tiny Swarovski crystal beads and silk bows. \
She is posed (permanent) on a heavy granit base.
