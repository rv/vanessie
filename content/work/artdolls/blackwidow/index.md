---
title: "Black Widow Lily And Her Little Dawn"
date: "2012-01-06"
available: false
---

Skull dolls

2012

- Lily = 75cm / 29.5 inches
- Dawn = 27cm / 10.6 inches

**Lily** \
Round wooden base with styrofoam cone, coated with pour-on rubber, as lower body. \
Upperbody is made from paperclay and apoxie over a metal frame, painted and sealed with acrylics and varnish. \
Her head is a real cat skull with real deer antlers and realistic BJD eyes and lashes. \
Viscose hair adorned with vintage milinary flowers. \
Lily's dress is made from lambs leather, vintage flower stamens, Swarovsky crystals, antique lace and silk ribbons. \
Her mantilla is made from black lace with a scalloped lace edge. \
She is holding a little vintage brass with mother of pearl bible with the complete first testament in it. \

**Dawn** \
Paperclay and apoxie over a metal frame for her body, painted and sealed with acrylics and varnish. \
Her head is made from a  real little turtle skull with tiny glass eyes and viscose hair. \
She's wearing a wool dress, off-white stockings, wool beret, velvet coat and leather strapped shoes. \
Dawn is holding her small teddybear.
