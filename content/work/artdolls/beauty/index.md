---
title: "Beauty Never Fades, It Only Changes"
date: "2012-06-04"
available: false
---

skull doll

2012

She's a skull covered in cobwebs but she is still beautiful, so getting older doesn't make your beauty fade. \
It is your actions which make you beautiful or ugly, not your skin or youth. \

H x W x D (approximately):
- 51 x 20 x 20 cm<
- 20.1 x 7.9 x 7.9 inch

The skull is a real Persian house cat skull with realistic acrylic BJD eyes and lashes. \
Her hair and antlers are made from high quality black English viscose adorned with Swarovski crystals. \
Her torso is sculpted with paperclay over a messing rod frame. \
The bust is dressed with black velvet and black antique lace from the Victorian era, finished with tiny Swarovski crystals and a metal pendant. \
She is posed (removable for save shipping) on an antique metal candlestick that can be placed on a massive granite base. \
She comes with black cobweb (new, not attached) so you can decide if you want to display her with or without the cobweb (see photo's). \
