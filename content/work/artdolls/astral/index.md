---
title: "Astral, She Who Controls the Stars and the Moon"
date: "2014-11-17"
available: false
---

2014

- Height 52 cm, 20.4 inch approx
- Diameter 28 cm, 11 inch approx

Astral her head is a sculpted paperclay unicorn head with a white translucent horn. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers acrylics, and then sealed with Mr. Clear. \
She is wearing a white 100% silk hand pleated dress covered with more then 4000 Swarovski clear crystals, one by one attached by hand, and trimmed with white faux couture fur at the hem. \
Her wings are real white dove wings. \
Astral  her hair is a very long ponytail made from a human hair extension and is also adorned with Swarovski crystal stars. \
In her hand she is holding a curtain of double Swarovski stars and a silver plated moon. \
She is posed on a hand turned wooden base in black satin finish. \
