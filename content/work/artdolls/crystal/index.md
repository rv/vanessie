---
title: "Crystal Growing Art"
date: "2017-03-04"
available: false
---

2017

The art of using chemistry to grow crystals on other materials, like bones, skulls, books etc, by using chemical solutions and seed crystals. \
The end result will be somewhat random but can be controlled more or less by placing seed crystals to form bigger crystals or use a solution (like boiling water) to shrink a larger formed crystal where the artist wants smaller crystals. \
The color of the crystals can be manipulated by using food coloring for example or, if you want more purity, by adding metals to the solution. \

H x W approx.
- 63 x 17 cm
- 25 x 6.7 inch

Crystal growing her head is a resin Vervet monkey skull (from a mold made of a real Vervet monkey skull) painted matte black and adorned with real amethyst crystals and flakes of 23 carat gold dust and a black human hair extension. \
Her dress is made of dark grey felt shaped in the form of a crystal and an diamond with a tiny machine embroidery, made by me, of a Hummingbird. \
She wears black stockings and is posed on a black wooden base.
