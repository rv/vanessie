---
title: "Mother"
date: "2010-10-25"
available: false
---

Skull doll

- 30.5 cm / 12 inch - total piece
- 27 cm / 10.8 inch - mother
- 11 cm / 4.3 inch - child

Mother's head is a skull from the Bearded Barbet (Lybius Dubius) with glass eyes, the tiniest golden lashes, and copper red high quality English viscose hair. \
Her body is made from stainless steel wire and apoxie skeleton with a paperclay finishing, painted with acrylics. \
Mother wears a dupioni silk hat with hand pleated satin ribbons and black tule. \
Her dress is made from beautiful high quality scalloped lace with satin ribbon and bows. \
The dress is completely lined with jersey. \
Her boots are sculpted from apoxie.

The little girl's head is a tiny (hummingbird?) skull with glass eyes, the tiniest golden lashes and copper red high quality English viscose hair just like her mother. \
Her body is made from stainless steel wire and apoxie skeleton with a paperclay finishing, painted with acrylics. \
The little girl is wearing a beautiful embroidered faded blue dress with a dupioni silk apron. \
On her feet she wears blue clocks and sculpted socks.

The little girl is playing a hop scotch game on earthware tiles. \
The 6" Walnut base is hand sanded and finished with high quality antique oil to give it a beautiful aged look.
