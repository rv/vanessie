---
title: "The McQueen Sisters"
date: "2012-03-25"
available: false
---

Skull dolls

2012

H x W x D (approximately):
- 28 x 32 x 25 cm
- 11 x 12.6 x 9.8 inch

The skulls are sculpted from paperclay and have handmade glass eyes finished with tiny lashes.
Their hair is made from high quality English viscose in a jet black color and one is adorned with a pink silk hairband.
The sisters are dressed in handmade clothes from light salmon antique silk with embroidery and pearls, dupioni silk and lambs leather adorned with black silk bows, black antique pique ribbon and tiny antique Victorian mourning jet beads. \
Both are wearing lamb leather tight high boots finished with metal buckles. \
The McQueen sisters are attached (non removable) to a high polished aluminum stone.
