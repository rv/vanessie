---
title: "Mme. Taille de Guepe"
date: "2013-08-08"
available: false
---

Skull doll

2013

H x W x D approx
- 40 x 20 x 15 cm
- 16 x 7.8 x 6 inch

The skull is a sculpted paperclay skull with a black sculpted mask. \
Her body is sculpted from paperclay over a metal frame, painted with acrylics and then sealed with Mr Clear. \
She is dressed in a silk cinching corset with a fur bottom. \
Her legs have black and yellow silk ribbons. \
Her hair is a ponytail made from a human hair extension. \
She is posed on a black double wooden base finished with a small black silk bow.
