---
title: "Tattoo Art"
date: "2017-03-04"
available: false
---

2017

A tattoo is a form of body modification made by inserting ink, dyes and pigments, either indelible or temporary, into the dermis layer of the skin to change the pigment.

H x W approx.
- 62 x 17 cm
- 24.4 x 6.7 inch

Tattoo has a real Vervet monkey skull (ethically obtained and with CITES papers) painted white (sponge technique) and with a hand painted swallow tattoo. \
She has glass eyes and a black human hair extension ponytail. \
Her body is sculpted from paper clay and painted white. \
Tattoo is wearing a white velvet hoodie dress with extra long sleeves, machine embroidered by me with different tattoo designs, and blue tights. \
She is posed on a black wooden base.
