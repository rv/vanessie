---
title: "The Cocteau Twins"
date: "2012-03-09"
available: false
---

Skull dolls

2012

H x W x D (approximately):
- 42 x 30 x 25 cm
- 16.5 x 11.8 x 9.8 inch

The skulls are sculpted from paperclay and have handmade glass eyes finished with tiny lashes. \
Their hair is made from high quality English viscose in a platinum white color and adorned with a black silk hairband. \
The twins are dressed in handmade clothes from dark salmon antique sari lace, black antique Victorian lace and lambs leather adorned with black silk bows and black antique pique ribbon. \
They both wear tiny fresh water pearl jewelery and a cameo brooch in gold plated setting. \
Both are wearing lamb leather ballet boots finished with metal buckles. \
The Cocteau Twins are attached (non removable) to a high polished aluminum stone.
