---
title: "Twice yours"
date: "2011-08-14"
available: false
---

Skull doll

2011

32 cm / 12.6 inch

Her head is made from a museum quality artificial Northern Cardinal skull from BoneClones, realistic acrylic eyes, black lashes, and high quality English viscose braided hair adorned with 2mm freshwater seed pearls and 3mm teardrop freshwater pearls. \
Her body is sculpted from Creative paper clay over a messing rod and tube skeleton covered with Aves apoxie for durability. \
She wears a beautiful silk brocade Victorian bridal jacket on top of her dupioni silk Victorian bridal dress. \
Underneath her dress she is wearing underwear, made from antique Victorian cotton and lace, and lace stockings. \
In her hands she is holding a wedding bouquet made from ceramic roses and antique Victorian wax flower buds and vines. \ 
The base is adorned with brass metalware and antique Victorian wax bridal flowers.
