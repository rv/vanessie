---
title: "Headmistress Grimm"
date: "2012-09-10"
available: false
---

Skull doll

2012

H x W x D (approximately):
- 29 x 12 x 16 cm
- 11.4 x 4.7 x 6.3 inch

The skull is an artificial Barred Owl skull from BoneClones with realistic acrylic BJD eyes and lashes. \
Her headpiece is made from brocade silk and velvet adorned with a metal chain and an antique gold plated pin with a garnet in the center. \
Her torso is sculpted with paperclay over a wooden frame. \
The bust is dressed with velvet, silk brocade, fur, embroidery silk, and black antique lace from the Victorian era, finished with a antique Victorian fob watch key. \
She is posed on a wooden and granite base.
