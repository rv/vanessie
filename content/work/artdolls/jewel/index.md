---
title: "Bejeweled Catacomb Saints"
date: "2017-03-04"
available: false
---

2017

The jeweled skeletons were originally found in catacombs beneath Rome in 1578, and distributed as replacements under the belief they were Christian martyrs to churches that had lost their saint relics in the Reformation. \
However, for most, their identities were not known. \
The receiving churches then spent years covering the revered skeletal strangers with jewels and golden clothing, even filling their eye sockets and sometimes adorning their teeth with finery. \
Yet when the Enlightenment came around they became a little embarrassing for the sheer amount of money and excess they represented, and many were hidden away or disappeared.

H x W approx.
- 63 x 23 cm
- 25 x 9 inch

Bejeweled Saint her head is made of a resin Vervet monkey skull (from a mold made of a real Vervet monkey skull) adorned with sculpted ornaments, jewels, fresh water pearls, a human hair extension ponytail and real 23 carat goldleaf/goldleaf powder. \
Her body is sculpted from paper clay and finished with goldleaf/goldleaf powder (23 carat). \
She is wearing a black felt dress and short cape, both machine embroidered by me, with gold metallic thread and adorned with many tiny pearl seed beads. \
Bejeweled Saint is posed on a black wooden base.
