---
title: "Wearing My Heart On My Sleeve"
date: "2015-04-04"
available: false
---

2015

Height approx 42 cm, 16.5 inch

Wearing My Heart On My Sleeve is sculpted from paperclay over a strong wire/messing rod and tube armature, painted with high quality acrylics and sealed with many layers Mr. Superclear. \
Her ears are gilded with 3 layers of 24.7 kt gold and her ponytail is a human hair extension. \
She is dressed in a handmade black spandex dress with a black flax and gold metallic silk. \
Her long sleeves are adorned with many gold beads. \
She is wearing her beating heart, made of translucent polymer, on her sleeve and comes with an extra 24.7 kt gilded golden extra heart so you can exchange the heart if you prefer.

Technical specs: \
The heart is lighted from the inside by an RGB LED with integrated driver chip. \
A self-programmed microcontroller drives the LED. \
The timing of the red 'lub-dub' and blue rest phases are carefully programmed. \
An international universal power adaptor is supplied. \
Please note that the light of the heart is not meant to be switched on for long periods, as the LED has limited lifetime (several hundreds of hours). \
Size of controller box: 50x35x20 mm
