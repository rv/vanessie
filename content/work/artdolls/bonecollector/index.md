---
title: "The Bone Collector"
date: "2011-02-20"
available: false
---

Wall mounted skull doll

2011

20 cm / 8 inch \

Parrot skull, glass eyes, English viscose hair with metal spring as hairclamp. \
Her body is apoxie over a metal wire skeleton. \
Lambs leather with feather clothes handmade by me. \
Inside the little metal with acrylic glass dome are small animal bones. \
