---
title: "Royal Blood"
date: "2011-09-24"
available: false
---

Skull dolls

2011

These three dolls are made after the Royal Blood series from Erwin Olaf with his kind permission.

==== Artificial skull doll Sissi \
17cm / 6.7 inch \
Resin skull with synthetic eyes and transparent white lashes. \
Hair is made from English viscose and tussah silk adorned with tiny fresh water pearls. \
Body is sculpted with paperclay and apoxie over a wire frame. \
Clothes are made from jersey and silk adorned with fresh water pearls and Swarovski crystals.

**Empress Sissi of Austria** \
24 December 1837 - 10 September 1898 \
Wife of Emperor Franz Joseph 1 of Austria. \
In 1898, despite warnings of possible assassination attempts, the sixty year old Elisabeth traveled incognito to
Geneva, Switzerland with Countess Irma Szt&aacute;ray de Szt&aacute;ra et Nagymih&aacute;ly. \
They were walking along the promenade when a twenty-five year old Italian anarchist named Luigi Lucheni
approached them, attempting to peer underneath the Empress' parasol. \
According to Sztaray, as the ship's bell announced the departure, Lucheni seemed to stumble and 
made a movement with his hand as if he wanted to maintain his balance. \
In reality, in an act of "propaganda of the deed", he had stabbed Elisabeth with a 4-inch long 
sharpened needle file which caused her dead.

==== Artificial skull doll Alexandra
17cm / 6.7 inch \
Resin skull with synthetic eyes and transparent white lashes. \
Hair is made from English viscose and tussah silk adorned with tiny fresh water pearls and faux pearls. \
Body is sculpted with paperclay and apoxie over a wire frame. \
Clothes are made from silk adorned with fresh water pearls and faux pearls. \
Her Russian religious artifact is made from a tiny metal cross and a 
vintage focal made from the Smithsonian museum shop. \
The necklace is made with lots of tiny freshwater pearls.

**Empress Tsarina Alexandra Feodorovna Romanova** \
6 June 1872 - 17 July 1918 \
Wife of Emperor Tsar Nicholas II of the Russian Empire. \
The former tsar and tsaritsa and all of their family, including the gravely ill Alexei, along 
with several family servants, were executed by firing squad and bayonets in the basement of the 
Ipatiev House, where they had been imprisoned, early in the morning of 17 July 1918, by a detachment 
of Bolsheviks led by Yakov Yurovsky. \
After all the victims had been shot, Ermakov in a drunken haze stabbed Alexandra's body and that of 
her husband, shattering both their rib cages and even chipping some of Alexandra's vertebrae.

==== Artificial skull doll Poppaea
17cm / 6.7 inch \
Resin skull with handmade glass eyes and transparent white lashes. \
Hair is made from English viscose and tussah silk adorned with tiny fresh water pearls. \
Body is sculpted with paperclay and apoxie over a wire frame. \
Clothes are made from silk adorned with satin ribbon and Swarovski crystals.

**Empress Poppaea Sabina the Younger** \
AD 30-65 \
Second wife of the Emperor Nero. \
The cause and timing of Poppaea's death is uncertain. \
According to Suetonius, while she was awaiting the birth of her second child in the summer of 65, she 
quarreled fiercely with Nero over him spending too much time at the races. \
In a fit of rage, Nero kicked her in the abdomen, so causing her death.
