---
title: "Her Secret Garden"
date: "2013-07-16"
available: false
---

Skull doll

2013

H x W x D approx
- 40 x 19 x 14 cm
- 16 x 7.4 x 5.5 inch

The skull is a sculpted paperclay skull with a black sculpted cat mask. \
Her body is sculpted from paperclay over a metal frame, painted with acrylics and then sealed with Mr. Clear. \
She is dressed in lace underwear with silk ribbons finished with metal buckles. \
Her (removable) saddle panniers are made of black haute couture fabric finished with synthetic hair and metal corset hook and eye. \
The inside of her panniers, her secret garden, is covered with black and golden fabric roses and two paper butterflies. \
She is posed on an ornate black double wooden base with tiny paperclay sculpted birds.
