---
title: "Ila"
date: "2012-10-11"
available: false
---

Skull dolls

2012

I want to share my thoughts about fur first, because I used real fur in this piece.

In my opinion fur is for animals and not for humans to wear. \
I would never tell someone what and what not to wear, that's not up to me. \
This means I would not tell someone not to wear fur but I would never ever wear it because knowing what happened to the animal the fur belonged to makes me sick to the stomach and nowadays we don't need to wear fur anymore, there are so many animal friendly alternatives to stay warm. \
Last year I bought a winter coat with what I thought was a fake fur collar, but it turned out to be real fur. \
I removed the collar from my coat and refused to wear it. \
Now for this piece, which I made with so much love and care, I used the fur from the collar, because in my opinion this was respectful use of the fur instead of throwing it away.

The two premature stillborn kitten skulls used in this piece were found in an Alaskan cat hoarders house by a rescue worker and sent to me to make something respectful with it. \
That is why I made this Yu'pik piece and in some way gave the fur back to an animal. \
Yu'pik were native Alaska eskimo who lived close to nature and used to hunt animals to survive, they used every part of the animal to not waste anything precious: the meat to eat, the fur/skin to stay warm in the extreme cold, and the bones to build all kind of things.

H x W x D approx.
- 23 x 11 x 25 cm
- 9 x 4.3 x 9.8 inch

Mother her skull is a tiny real stillborn kitten skull with English viscose braids. \
Her body is sculpted from paperclay over a metal frame. \
She is wearing clothes made from leather and fur. \
Mother is permanently attached to the sled.

The child her skull is the sibling skull from the mother (also real and stillborn). \
She is wearing a real leather and fur sled sleeping bag adorned with antique African trading beads. \
Child can be removed from the sled.

The sled is made of all different kind of bones (most chicken), apoxie, leather straps, and is adorned with pieces of porcupine needles, african trading beads, and fur.
