---
title: "Into The Forest"
date: "2012-12-04"
available: false
---

Wall mount

2012

H x W x D approx.
- 35 x 25 x 15 cm
- 13.7 x 9.8 x 5.9 inch

The skull is a real house cat skull with branch antlers made of paperclay over metal wire and painted with acrylics. \
There's a tiny black porcelain bird on one of the branches. \
Her eyes are realistic BJD eyes and she wears fake eyelashes. \
I used high quality English viscose for her hair and she is painted with acrylics. \
On her paperclay torso she wears a fur coat with a fur fox stole. \
The fox head is made from paperclay and painted with acrylics. \
Her hands are real animal bone claws. \
She is attached on a black velvet background in a deep, black wooden frame.
