---
title: "Scarlett O'Hara"
date: "2011-10-14"
available: false
---

Skull doll

30 cm / 11.8 inch

Her head is made from a hand casted artificial Magpie skull aged with brown patina. \
The eyes are handmade glass eyes with brown lashes, and high quality English viscose hair adorned with freshwater pearls. \
Her body is sculpted from Creative paper clay over a messing rod and tube skeleton covered with Aves apoxie for durability. \
She wears a beautiful velvet dress with 3 layers of skirts. \
Underneath her skirts she is wearing an underskirt and bloomers. \
There's a chain with her scissor, watch and spoon hanging between the layers of her skirt, a so called chantelaine. \
Scarlett also wears lambs leather boots. \
The base is adorned with brass filigree, velvet, and a freshwater pearl.
