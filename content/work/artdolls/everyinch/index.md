---
title: "Every Inch A Lady"
date: "2010-06-18"
available: false
---

Skull doll

2010

75 cm / 29.5 inch

Her skull is made of paperclay on apoxie and metal wire mesh and she's got realistic eyes. \
Her body is made of apoxie over a metal rod frame and she is made in 3 separable parts for easy shipment (head, torso, lower body).
She is painted with acrylics. \
Her dress is made of dupioni silk, georgette silk and crushed silk. \
The hairdo is in Japanese style and made from a mix of viscose and mohair. \
She wears high leather boots.
