---
title: "Paula and Pauline - Conjoined Twins 2"
date: "2012-11-03"
available: false
---

Wall mount

2012

- 30 x 35 x 5 cm
- 11.8 x 13.7 x 1.9 inch

The conjoined skull of Paula and Pauline is made from two resin cast owl skulls and jointed with apoxie. \
Her eyes are two different types of realistic BJD eyes and she wears fake tiny eyelashes. \
I used high quality English viscose for her hair and she is painted with acrylics. \
On her paperclay torso she wears a velvet shirt, a collar from antique Victorian lace with a beautiful antique scarf pin. \
Her stole is made from recycled fur scraps and she wears a necklace from golden sead fresh water pearls. \
The skull is attached on a bordeaux red velvet background in a gorgeous antique gesso frame. \
The velvet used is a high quality silk/cotton velvet.
