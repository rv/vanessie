---
title: "Release Me!"
date: "2009-10-17"
available: false
---

Skull doll

2009

22 cm / 8.7 inch

Bird skull, apoxie, viscose hair, leather boots, jersey tricot, satin, tule, wooden base, vintage clockwork, metal springs.
