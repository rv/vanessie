---
title: "Eden"
date: "2013-06-19"
available: false
---

Skull doll

2013

H x W x D approx.
- 45 x 20 x 18 cm
= 17.7 x 8 x 7 inch

The skull is a sculpted paperclay skull with closed eyes and covered with stiffened antique black lace. \
Her body is sculpted from paperclay over a metal frame, painted with acrylics and then sealed with Mr. Clear. \
She is dressed in lace underwear with silk ribbons and on the back an antique Victorian mourning bead. \
Her saddle pannier is made of black haute couture fabric finished with synthetic hair and metal stirrups. \
Eden is holding a golden paperclay apple in her hand. \
She is posed on a ornate black double wooden base.
