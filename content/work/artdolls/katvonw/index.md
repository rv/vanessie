---
title: "Kat von W."
date: "2011-03-09"
available: false
---

Skull doll

58 cm / 22.8 inch (incl her double base)

Let me introduce to you member three from the Psychobilly gang: Kat von W. \
Kat's official name is Cat von Wildthing, but she prefers to be called Kat von W. \
Kat is made of apoxie and paperclay over a metal wire and rod/tube frame. \
Her beautiful latina head is a real cat skull. \
She is painted with acrylics and have realistic synthetic eyes. \
Her hair is real human black hair and her shirt and skirt are made from silk. \
She also wears real nylons and leather strapped shoes.
