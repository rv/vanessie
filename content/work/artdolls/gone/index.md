---
title: "Gone"
date: "2009-10-03"
available: false
---

Emotion doll

14 cm / 5.5 inch

Polymer, mohair hair, antique fabric, lace, beads, glass/metal. \
Extra feature: fabric used is from an antique Victorian mourning jacket (late 1800), jewel case and mourning locket are also Victorian antiques (mid to late 1800).
