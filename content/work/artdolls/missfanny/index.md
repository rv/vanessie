---
title: "Miss Fanny Lacroix"
date: "2013-01-25"
available: false
---

Skull doll

2013

H x W x D approx.
- 35 x 26 x 15 cm
- 13.7 x 10.2 x 5.9 inch

Miss Fanny Lacroix her skull is a stillborn Labrador puppy skull with brown glass eyes and tiny lashes. \
Her hair is made from high quality black English viscose. \
Miss Fanny her body is sculpted from paperclay over a metal frame, painted with acrylics, "tattooed" with sharpies and watercolor pencils, and then sealed with Mr. Clear. \
She is dressed in damask corset, girdle and a pocket cage crinoline adorned with black and powder pink silk ribbon. \
Her stockings and knicker are made from lace and netting. \
Miss Fanny her boots are made of soft lambs leather, and she is posed (permanently) on a black wooden base.
