---
title: "Rising"
date: "2012-11-09"
available: false
---

Skull doll

2012

H x W x D approx.
- 28 x 20 x 20 cm
- 11 x 7.9 x 7.9 inch

Rising her skull is a real sparrow hawk skull with handmade glass eyes and tiny lashes. \
Her hair is made from high quality black English viscose adorned with golden pearls and flowers. \
Her body is sculpted with paperclay over a metal frame. \
Rising is dressed in antique pink vintage embroidered silk with seed beads, gold sequins, silk bows, and many layers of black quality tulle. \
She is posed permanently on a heavy granite base.
