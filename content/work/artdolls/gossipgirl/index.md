---
title: "The Gossip Girl"
date: "2009-10-01"
available: false
---

Skull doll

2009

42 cm /16.5 inch

Guinea pig skull, apoxie, silk, lace, satin flowers, feathers, seed beads, oxbone skull beads, antique mourning brooch, LED light. \
Extra feature: antique Victorian mourning brooch from mid to late 1800.
