---
title: "I Pour My Heart Out"
date: "2015-04-04"
available: false
---

2015

Size approx 42 by 30 cm, 16.5 by 11.8 inch

I Pour My Heart Out is sculpted from paperclay over a strong wire/messing rod and tube armature, painted with high quality acrylics and sealed with many layers Mr. Superclear. \
Her ears are gilded with 3 layers of 24.7 kt gold and her ponytail is a human hair extension. \
She is dressed in a handmade black spandex dress with silk ribbons. \
Her long sleeves are adorned with many gold beads. \
She is wearing her beating heart, made of translucent polymer, on her sleeved hand and comes with an extra 24.7 kt gilded golden heart so you can exchange the heart if you prefer.

Technical specs: \
The heart is lighted from the inside by an RGB LED with integrated driver chip. \
A self-programmed microcontroller drives the LED. \
The timing of the red 'lub-dub' and blue rest phases are carefully programmed. \
An international universal power adaptor is supplied. \
Please note that the light of the heart is not meant to be switched on for long periods, as the LED has limited lifetime (several hundreds of hours). \
Size of controller box: 50x35x20 mm
