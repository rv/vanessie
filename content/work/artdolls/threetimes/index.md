---
title: "Three Times A Lady"
date: "2012-08-29"
available: false
---

2012

H x W x D (approximately):
- 12.5 x 9 x 9 cm
- 4.9 x 3.5 x 3.5 inch

Acrylic dome with three tiny real birdskulls. \
The skulls have glass eyes and English viscose hair adorned with pearls, Swarovsky crystals and silk ribbon bow. \
The collars are made from antique lace and have adornments of fresh water pearls, Swarovsky crystal and an acrylic cameos. \
The dome is not removable.
