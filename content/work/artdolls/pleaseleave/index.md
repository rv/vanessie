---
title: "Please Leave!"
date: "2009-12-11"
available: false
---

Skull doll

2009

26 cm / 10.2 inch

Agapornis skull, apoxie, miniature bricks and tiles, flints/pebbles, leather, jersey, dried walking leave, paper roses, wooden base. \
Painted with acrylics and finished with 3DG Art gel.
