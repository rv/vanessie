---
title: "Snow White's Poisoned Apple"
date: "2012-04-15"
available: false
---

Skull bust

2012

H x W x D (approximately):
- 28 x 10 x 15 cm
- 11 x 3.9 x 5.9 inch

The skull is a real Persian house cat skull with realistic acrulic BJD eyes and lashes. \
Her hair is made from high quality black English viscose adorned with a red silk hair bow. \
The bust is dressed with black velvet, red dupioni silk and black antique lace from the Victorian era, finished with tiny black sead beads and red Swarovski bicones. \
She's holding a poisoned apple in her hand made of paperclay. \
Her granite base is adorned with selfmade pure silver birds and metal 'tree' feet.
