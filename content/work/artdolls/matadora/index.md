---
title: "Matadora"
date: "2013-11-12"
available: false
---

Who's in charge now?

2013

- Height 41 cm, 16 inch approx
- Diameter 17 cm, 6.7 inch approx

Matadora her head is a sculpted paperclay bull head with a black face and real golden horns. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers of acrylics, and then sealed with Mr. Clear. \
She is wearing a black velvet bolero with goldwork bullion embroidery, a 100% silk hand pleated top, and a black velvet hotpants with golden bows. \
Her legs are painted black with black and golden silk bows. \
Matadora her hair is a base long ponytail made from a human hair extension. \
In her hand she is holding a banderillas from metal with silk ruffle decoration. \
She is posed on a hand turned wooden base in black satin finish. \

I say no to bull fights!
