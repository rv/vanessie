---
title: "Caged Beauty #2 Rei"
date: "2014-01-06"
available: false
---

2014

- height 45 cm / 17.7 inch
- diameter 18 cm / 8 inch

Rei her head is a sculpted paperclay skull head with a black fox mask. \
The back of her ears are gilded with real 23.75 karat gold. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers acrylics, and each layer also sealed with Mr. Clear. \
Rei is wearing black silk velvet hotpants with a silk dress, Victorian style but designed in a modern way, adorned with gold metal beads and silk ribbon. \
Her legs are painted black with black silk ribbon lines and golden tiny pearls. \
Rei is also wearing a handmade cage crinoline with silk bows and golden breads. \
She has a very long ponytail made from a human hair extension. \
Her slender skeleton hands are finished with the top of her fingers gilded with real 23.75 karat gold. \
She is posed on a hand turned wooden base in black satin finish.
