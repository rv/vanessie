---
title: "Born To Be Wild"
date: "2013-11-19"
available: false
---

2013

- Height 42 cm, 16.5 inch approx
- Diameter 19 cm, 7.4 inch approx

Born To Be Wild her head is a sculpted paperclay elephant head with a black face and real golden tusks. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers acrylics and then sealed with Mr. Clear. \
She is wearing a black 100% silk velvet bolero with golden epaulettes and long slips at the back, and a black 100% silk velvet high waist hotpants with golden buttons. \
Her pannier is also made of 100% silk velvet and finished with a golden ribbon and an edge of human hair extensions. \
Her legs are painted black with black/gold silk bows and a savannah silhouette lace edge. \
Born To Be Wild her hair is a base long ponytail made from a human hair extension. \
In her hand she is holding a leather whip. \
She is posed on a hand turned wooden base in black satin finish. \

I say no to wild animals held in captivity!

