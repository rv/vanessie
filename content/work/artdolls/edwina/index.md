---
title: "Edwina"
date: "2013-03-04"
available: false
---

Skull doll

2013

Doll + dollhouse \
Height approx.
- 33 cm - 12.9 inches

No. 2 in a series of three.

Edwina's skull is an artificial kitten skull with bright blue glasseyes and tiny lashes. \
She wears a chocolate brown viscose "wig" with braids and silk bows. \
Edwina's body is made of paperclay over a metal and tin foil frame and her fingers are made of apoxie sculpt.

The clothes are made of cotton, velvet, and lace with tiny gold buttons, aged with coffee and sanding paper to give it an old worn look, and embroidered by me with tiny roses. \
Her boots are made from aged lambs leather and has tiny leather laces.

The dollhouse is made of wood and painted with different medium to give it a used antique look. \
It's decorated with the tiniest sturdy cardboard furniture. \
Edwina is pushing her dollhouse by a wooden bar which is permanently attached to her hands and can be attached to the dollhouse by a rod/hole construction.
