---
title: "Fossette La Toutouque"
date: "2010-08-23"
available: false
---

Skull doll

2010

Let me introduce to you: Mdm Fossette La Toutouque. \
Fossette's head is a real cat skull painted gold with high quality gold paint. \
She is 59cm/23.2inch big and wears a silk brocade trouser and shirt with a lace collar. \
Her over coat is made of silk taffeta lined with silk brocade and fastened with golden and black pearls. \
Fossette's high heeled boots are made from soft deer leather with (faux)golden buttons. \
Her hat is made from felted wool with beautiful feathers, golden mini pearls and buttons. \
Fossette leans on a posh cane topped with a huge diamond.
