---
title: "Calla"
date: "2012-01-20"
available: false
---

Skull doll

2011

48 cm / 18.9 inch

Owl skull sculpted from Paperclay with beautiful handmade glass eyes, and viscose hair. \
Her hat is made from a wool felt, satin pleated ribbon, feathers, Victorian mourning decoration ornament, metal chain, and Swarovski cones. \
She wears silk handmade clothes over a paperclay/apoxie body, which is sculpted over a metal rod and tube armature. \
Her boots are made from very soft roughed leather, red embroidery silk an red painted fresh water pearls. \
The base is polished granite with metal feet. \
She is holding a metal saber (vintage collectable Toledo saber) and a lambs leather glove.
