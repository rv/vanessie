---
title: "Queen, Pawn, and Knight"
date: "2012-07-11"
available: false
---

Skull dolls

2012

==== Queen
13 cm / 5.1 inch (without the stone base) \
Real bird skull on a paper clay bust with English viscose hair and Swarovski crystals. \
Her glass eyes are inserted with apoxie sculpt. \
She is dressed with antique lace and soft lambs leather with Swarovski crystal accents and she is wearing a collar with fur and a Swarovski crystal. \
Her metal base is a piece from an antique candle holder and is standing on a vintage nature stone base.
==== Pawn
11 cm / 4.3 inch (without the stone base) \
Real bird skull on a paper clay bust with English viscose hair. \
Her glass eyes are inserted with apoxie sculpt. \
She is dressed in soft lambs leather. \
Her metal base is a piece from an antique candle holder and is standing on a vintage nature stone base.
==== Knight
12 cm / 4.7 inch (without the stone base) \
Real bird skull on a paper clay bust with English viscose hair in a high ponytail. \
Her glass eyes are inserted with apoxie sculpt. \
She is dressed with antique lace and soft lambs leather with metal accents. \
Her metal base is a piece from an antique candle holder and is standing on a vintage nature stone base.
