---
title: "The Creature From The Black Leather Lagoon"
date: "2011-03-05"
available: false
---

Skull doll

2011

56 cm / 22 inch

Snapping turtle skull with glass eyes on a apoxie body sculpted over a metal wire/rod and tube frame. \
He wears a jersey t-shirt with a The Cramps album cover(printed on silk) on it, jeans, a leather jacket and Doc Martens. \
All clothes are handmade by me. \
His guitar is a miniature Fender. \
He is securely attached (not removable) to a double, solid wooden base covered with black and white painted tiles.
