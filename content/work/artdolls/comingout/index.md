---
title: "Coming Out Of The Dark"
date: "2010-01-21"
available: false
---

Skull doll

2009

27 cm / 10.6 inch

Coming out of the dark is made of an Agapornis skull. \
Her body is completely made of apoxie over a stainless steel wire armature. \
Her hair is tibetan lamb hair and her clothes and boots are made by me from jersey, elastic ribbon and leather. \
Her base is made from miniature bricks on a wall of apoxie finished with grout and a "gold" painted frame. \
The bottom base is made of wood finished with ceramic tiles, flints/pebbles, polymer roses and dried moss. \
The chains and chain stands are metal with apoxie ball on top. \
Painted with acrylics and finished with 3DG Art gel.
