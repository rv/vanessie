---
title: "Losing A Friend"
date: "2012-09-10"
available: false
---

Skull doll

2012

H x W x D (approximately):
- 48 x 24 x 24 cm
- 18.9 x 9.4 x 9.4 inch

The skull is a real Persian house cat skull with realistic acrylic BJD eyes and lashes. \
Her hair is made from high quality auburn English viscose, and she is wearing a bonnet made from antique Victorian lace, adorned with silk ribbon and silk embroidered roses. \
Her body is sculpted with paperclay over a messing rod frame with apoxie for extra strength. \
She is dressed in a cotton dress adorned with antique laces from the Victorian era, silk ribbons, and the tiniest buttons. \
Under her dress she is wearing an underskirt from antique lace and a beautiful skincolored bloomer. \
Her legs are covered in cotton stockings, and she is wearing leather and metal polio braces and shoes.

She's holding a 3” handmade teddybear, made to look old and used.

Her 'dead' stuffed needle felted hare is missing one paw. \
It is tied to the wooden cart she is pulling with a leather belt.

The wooden base is adorned with hand grinded stones and sand, aged with patina. \
The doll can be removed from her base but is not free standing.
