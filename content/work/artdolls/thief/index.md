---
title: "Thief Of Hearts"
date: "2011-07-23"
available: false
---

Skull doll

2011

25 cm / 9.8 inch

Her head is made from a museum quality artificial Burrowing Owl skull from BoneClones. \ 
realistic glass eyes, black lashes, and English viscose braided hair in a black hair net. \
She wears a beautiful black felt Victorian riding hat on her head (attached to her hair). \
Her body is sculpted from Creative paper clay over a messing rod and tube skeleton covered with Aves apoxie for durability. \
She is wearing lace underwear and stockings underneath her dupioni silk dress with embroidery. \
Her Victorian riding jacket is made from black velvet with two part sleeves, two point collar and a double row of pearl buttons. \
She also wears a beautiful lace collar with a cameo set in a gold tiffany setting. \
In her hands she is holding a golden cage with a polymer heart painted with Genesis heat set paints in it. \
Her riding boots are made from soft lambs leather with pearl buttons.
