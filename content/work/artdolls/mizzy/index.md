---
title: "Mizzy's Day Off"
date: "2013-04-21"
available: false
---

Skull doll

2013

H x W x D approx
- 42 x 18 x 18 cm
- 16 x 7 x 7 inches

Mizzy's skull is a paperclay labrador pup skull with closed eyes, clowns make-up and tiny lashes. \
Her hair is made from high quality white English viscose with red silk bows and a wool felt clown's hat adorned with fresh water pearls and a red Swarovski crystal bead. \
Mizzy's body is sculpted from paperclay over a metal frame, painted with acrylics and then sealed with Mr. Clear. \
She is wearing a silk dress with a tulle petticoat, a striped knicker, striped and dotted nylon arm warmers and stockings. \
Mizzy is holding her red clown's nose in her articulated apoxi fingers and is wearing leather roller skates. \
Her necklaces are made of tiny fresh water pearls, black Swarovski crystals, polished red coral beads, and gold plastic beads. \
She is posed (permanently) on a double wooden base with a gold metal name tag.
