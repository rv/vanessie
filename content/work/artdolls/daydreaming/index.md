---
title: "Daydreaming"
date: "2010-05-19"
available: false
---

2010

Collaboration with <a href="http://www.kamillameesters.com/">Millie Meesters</a>. \
Based on <a href="http://lunebleu.deviantart.com/art/Midnight-offering-155181239">Midnight offering from Ana Cruz</a>. \
Piece is made as donation for the <a href="http://www.pinkribbon.nl/">Pink Ribbon</a> lottery which was held in October 2010 during the <a href="http://www.niesjewolters.nl/index.php?option=com_content&task=view&id=67&Itemid=32">Ahoy Dollshow</a>, Rotterdam The Netherlands. \

- W 16.5 inch / 42 cm<
- D 5.5 inch / 14 cm
- H 11.4 inch / 29 cm

The doll would be 54 cm when standing.
