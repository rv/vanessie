---
title: "Edward"
date: "2013-03-04"
available: false
---

Skull doll

2013

Doll \
Height approx.
- 33 cm - 12.9 inches

Dollhouse on wheels \
H x W x D approx.
- 19 x 19 x 10 cm
- 7.4 x 7.4 x 3.9 inch

No. 1 in a series of three.

Edward's skull is an artificial kitten skull with bright blue glass eyes and tiny lashes. \
He wears a white viscose "wig" with a little ponytail and silk bow in an Edwardian style. \
Edward's body is made of paperclay over a metal and tin foil frame, and his fingers are made of apoxie sculpt. \
His wooden leg is also paperclay painted with acrylics, patina and high gloss varnish. \
The clothes are made of antique cotton, velvet, alcantra and lace with tiny gold buttons, aged with coffee and sanding paper to give it an old worn look. \
Edwards shoe is made from alcantra and has a silk bow with metal buckle.

The dollhouse is made of wood and painted with different medium to give it a used antique look. \
It's decorated with the tiniest sturdy cardboard furniture. \
There are two tiny porcelain Huskies sitting on the front porch.
