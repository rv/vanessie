---
title: "Soleil Noir"
date: "2012-09-28"
available: false
---

Skull doll

2012

H x W x D approx
- 55 x 30 x 20 cm
- 21.6 x 11.8 x 7.8 inch

Soleil Noir her head is made from a real Persian cat skull painted high gloss black with quality spray paint and then protected with high gloss varnish. \
Her hair is made from English viscose styled in a mohawk. \
Soleil Noir her super tight suit is made from very soft lambsleather and ends in flat ballet boots. \
She is tied with red nylon rope to a black wooden ring. \
The pose and bindings are inspired by a photo from Douglas Kent's book "Complete Shibari Vol. 1: Land". \
Soleil Noir's base is a solid rough oak wood base and is very heavy (complete piece weights 7 kilo).
