---
title: "Dark Bride"
date: "2010-08-08"
available: false
---

Skull doll

2010

70 cm / 27.6 inch

Her skull is sculpted from apoxie with a paperclay layer and painted with acrylics. \
Her body is also made from apoxie and sculpted in two parts so she is seperatable at her waist (rod/tube) for safer shipment. \
She's got glass eyes and mohair/sheep wool/viscose hair. \
I used 100% dupioni silk and 100% crepe georgette silk for her dress and vale, and lace and Swarovski crystals on her underskirt. \
The roses are Mulberry paper flowers.
