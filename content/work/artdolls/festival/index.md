---
title: "Festival Of Colours"
date: "2015-09-02"
available: false
---

2015

Height approx 29 cm, 11.4 inch

Made of paperclay, painted with acrylics, and finished with many layers Mr. Super Clear. \
Festival Of Colours her mask is inspired on antique Chinese dance masks and, is made of flax fabric, hand embroidered with 100% silk Japanese embroidery thread. \
Her dress is also made of flax fabric, and has a silk chiffon ruffled underskirt. \
The dress is hand embroidered with countless seed beads, manually applied one by one. \
She has human hair extensions with two jointed koi carp as embellishment. \
Festival Of Colours is posed on a black wooden base finished with alcantara fabric and a small golden ribbon.
