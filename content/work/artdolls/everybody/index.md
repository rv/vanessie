---
title: "Everybody Wears A Mask"
date: "2016-06-05"
available: false
---

2016

H x W x D approximately
- 65 x 36 x 20 cm
- 25.5 x 14 x 7.8 inch

This doll is sculpted from Creative paper clay over a strong messing rod and tube armature, painted and then sealed with many layers of Mr. Superclear UV flat. \
Her hair is a real human hair extension. \
She is dressed in a wool felted dress with panniers. \
The dress is hand embroidered with tiny gold plated seed beads, fresh water pearls, metal sequins, and 4 real raw black diamonds (2.04 ct each, and conflict free). \
She wears real nylon tights with silk ribbons and bows. \
Her mask is sculpted from Creative paper clay. \
She can hold the mask in her hand, or wear it on her face. \
The puppy dog is sculpted from Creative paper clay with a real stillborn puppy skull, painted and sealed with Mr. Superclear UV flat, and wears a Swarovski crystal collar. \
They are posted (removable) on a olive wooden base, the doll with rods for a sturdy positioning, the dog free standing.
