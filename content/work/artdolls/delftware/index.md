---
title: "Delftware/Kintsugi"
date: "2017-03-04"
available: false
---

2017

Delftware or Delft pottery, also known as Delft Blue, is blue and white pottery made in and around Delft in The Netherlands and the tin glazed pottery made in the Netherlands from the 16th century.

Kintsugi ("golden joinery"), also known as Kintsukuroi ("golden repair"), is the Japanese art of repairing broken pottery with lacquer, dusted or mixed with powdered gold, silver, or platinum, a method similar to the maki-e technique. \
As a philosophy, it treats breakage and repair as part of the history of an object, rather than something to disguise.

H x W approx.
- 61 x 17 cm
- 24 x 6.7 inch

Delftware has a resin Vervet monkey skull (from a mold made of a real Vervet monkey skull) painted white with blue Delftware style handpainted flowers, 23 carat gold dust kintsugi lines, and a black human hair extension. \
Her body is sculpted from paper clay and painted white. \
She wears a dress and stockings made from my own designed fabric, I transferred a photo of our Husky Toyah (2005-2016) into a Delft blue tile design and had it printed on Taj silk and organic jersey. \
She is posed on a black wooden base.
