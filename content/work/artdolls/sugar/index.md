---
title: "Sugar and Spice"
date: "2011-06-13"
available: false
---

Skull dolls

2011

32 cm / 12.6 inch total height

**Sugar(()) (standing doll) is made of: \
Artificial museum quality Burrowing Owl skull from BoneClones, realistic highly detailed eyes, high quality English viscose hair, handmade fascinator from leather/feathers/Russian veiling. \
Her body is made of paperclay and apoxie sculpt, 1 mm pearl choker with cameo, handmade clothes from different kinds of silk and exclusive lace.
Her corset and skirt are trimmed with antique Victorian mourning Whitby jets and an antique Victorian jet mourning piece. \
Underneath her clothes she wears leather boots, striped stockings and a black bloomer with ruffles.

**Spice** (kneeling doll) is made of: \
Artificial museum quality Yellow-Billed Cuckoo skull from BoneClones, realistic highly detailed eyes, high quality English viscose hair cut in a gothic bob, handmade miniature top hat from leather and feathers. \
Her body is made of paperclay and apoxie sculpt painted with acrylic paints and sealed with Mr clear UV. \
Handmade clothes from leather, exclusive lace and rabbit fur trimmings. \
Her boots are made of very thin leather and apoxie sculpt. \

The beautiful wooden base (14cm / 5.5 inch square) is decorated with painted and distressed terracotta tiles, artificial grass and cats (painted with black acrylics).
