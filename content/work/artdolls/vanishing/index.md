---
title: "Vanishing"
date: "2012-09-26"
available: false
---

Skull doll

2012

H x W x D approx.
- 24 x 20 x 20 cm
- 9.4 x 7.9 x 7.9 inch

Vanishing her skull is a real Crested Oropendola (Panama) bird skull with glass eyes and high quality English viscose hair. \
Her body is sculpted with paperclay over a strong metal armature, painted with acrylics and sealed with artist gel varnish. \
Vanishing is dressed with antique Victorian lace, silk and tulle adorned with embroidery and silk ribbon. \
She's wearing a necklace from tiny real rough black diamonds and fresh water pearls and a ring from a little bigger real rough white diamond. \
In her braided hair is an vintage indian bridal bead used as hairpin. \
Her base is a Belgium fossil stone on which she is permanently attached.
