---
title: "Caged Beauty #1 Valentina"
date: "2014-01-22"
available: false
---

2014

- height 43 cm / 17 inch
- diameter 16 cm / 6.3 inch

Valentina her head is a sculpted paperclay skull head with a black ram mask with gilded (real 23.75 karat gold) horns. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers of acrylics and each layer also sealed with Mr. Clear. \
Valentina is wearing black silk velvet hotpants with a black silk velvet and micro suede military style coat adorned with gold colored half pearls, gold colored ribbons and finished with a half cage pannier. \
Her legs are painted black with black silk ribbon bows. \
She has a very long ponytail made from a human hair extension. \
Her slender skeleton hands are finished with the top of her fingers gilded (also with real 23.75 karat gold). \
She is posed on a hand turned wooden base in black satin finish.
