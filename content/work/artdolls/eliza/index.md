---
title: "For My Name Is Eliza Day"
date: "2012-08-15"
available: false
---

Skull doll

2012

H x W x D approximately
- 7 x 33 x 23 cm
- 2.8 x 13 x 9.1 inch

Inspired by the song <i>Where The Wild Roses Grow</i> from Nick Cave. \
Eliza Day her skull is a real Agarponis skull with glass eyes and lashes. \
Her hair is made from high quality black English viscose. \
Her body is sculpted with paperclay over a metal frame. \
She is dressed in a black stiffened silk dress finished with tiny Swarovski crystal beads and given a wet look with high gloss varnish. \
She is posed (permanently) on a heavy Belgium fossil stone base adorned with roses, black sand and fake water (high gloss varnish).
