---
title: "Poison Ivy"
date: "2012-01-20"
available: false
---

Skull doll

2012

50 cm / 19.7 inches

Granite base with decorated metal feet. \
Body is made from Paperclay and apoxie over a metal frame, painted and sealed with acrylics and varnish. \
Her head is an owl skull sculpted with Paperclay, handmade glass eyes, and lashes. \
Viscose hair adorned with many mulberry flowers. \
Poison Ivy's dress is made from lambs leather, silk organza, mulberry flowers, lace, and silk ribbons. \
She is holding a Poison Ivy flower made from antique Victorian wax flowers.
