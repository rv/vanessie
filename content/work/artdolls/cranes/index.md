---
title: "A Thousand Cranes"
date: "2013-04-24"
available: false
---

Skull doll

2013

H x W x D approx
- 30 x 30 x 13 cm
- 12 x 12 x 5 inches

Her skull is an artificial resin kitten skull with realistic blue eyes and tiny lashes. \
Her hair is made from high quality black English viscose with on the other side of her skull a tattoo. \
A Thousand Cranes her body is sculpted from paperclay over a metal frame, painted with acrylics and then sealed with Mr. Clear. \
She is dressed in antique black lace with an organza bloomer and red silk ribbons, and is wearing black nylons and black leather ballet slippers. \
She is posed (permanently) on a black wooden base adorned with black miniature gravel, and red paper origami cranes on red nylon rope with wire inside.
