---
title: "Savage Beauty"
date: "2012-04-07"
available: false
---

Inspired by Alexander McQueen

Skull dolls

2012

H x W x D (approximately):
- 45 x 26 x 26 cm
- 15.7 x 10.2 x 10.2 inch

The skulls are sculpted from paperclay and have handmade glass eyes finished with tiny lashes. \
Their hair is made from high quality English viscose in a chocolate and auburn color. \
The dolls are dressed in handmade clothes from black dupioni silk adorned with yellow/gold silk soutache ribbon and lambs leather adorned with embroidery, pheasant and ostrich feathers, and tiny paperclay bird skulls. \
Both are wearing thigh high lamb leather ballet boots. \
The accessory golden mouse in Alexander McQueen style is made from a real house mouse skeleton with Swarovski crystal eyes. \
The dolls are attached (non removable) to a Belgian hardstone base (heavy).
