---
title: "Tears Of The Saints"
date: "2011-07-09"
available: false
---

Skull doll

2011

40cm / 15.7 inch from the top of her halo to the bottom of her base

Her head is made from a museum quality artificial Short Eared Owl skull from BoneClones, \ 
realistic glass eyes, individual lashes, and English viscose braided hair with a metal halo. \
She wears a beautiful lace mantilla on her head (sewed to her hair). \
Her body is sculpted from Creative paper clay over a messing rod and tube skeleton covered with Aves apoxie for durability. \
She is wearing lace underwear and stockings underneath her dupioni silk pleated dress with double sleeves. \
In her hands she is holding a faux pearl <a href="http://www.paternoster-row.medievalscotland.org/" target="_blank">Paternoster rosary</a>. \
The wooden base is painted with high quality acrylics and then decorated with mosaic tiles, faux pearls, and golden trimmings.
