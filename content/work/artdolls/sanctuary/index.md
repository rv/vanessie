---
title: "She Sells Sanctuary"
date: "2010-02-09"
available: false
---

Skull doll

2009

22 cm / 8.7 inch

She Sells Sanctuary is made of a bird skull and her body is made of apoxie on a stainless steel armature. \
Her hair is high quality English viscose and her clothes are handmade by me from vintage fabric (from late 1800), lace and gold brocade. \
The wall is made of miniature bricks, grout, apoxie, miniature fountain with faux water, dried roots and polymer/paper roses. \
Her wooden base is covered with ceramic tiles, sand, dried moss, faux grass and a miniature birdbath.
