---
title: "Tell Me Why"
date: "2011-02-21"
available: false
---

Wall mounted skull doll

2011

14 cm / 5.6 inch

Artificial museum quality owl skull, glass eyes, English viscose hair with hand dyed silk bows. \
Paperclay body over a metal wire skeleton with metal hook for wall hanging in her back. \
Nicky velvet shirt, brown leather waistband with gold colored buttons. \
Over shirt from silk/velvet mix. \
The baby is made from mixed animal bones.
