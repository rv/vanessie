---
title: "And The Sun Will Rise"
date: "2013-12-14"
available: false
---

- height 41 cm / 16 inch
- diameter 17 cm / 6.7 inch

And The Sun Will Rise her head is a sculpted paperclay lionhead with a black face and golden ears. \
Her body is sculpted from paperclay over a metal frame, painted with many thin layers acrylics, and each layer also sealed with Mr. Clear. \
And The Sun Will Rise is wearing a high waist hotpants with golden buttons and black/gold beaded Masaai jewellery. \
Her legs are painted black with black silk bows and a safari animals silhouette edge. \
She has a very long ponytail made from a human hair extension. \
In her hand she is holding a Masaai spear with hair and beads. \
She is posed on a hand turned wooden base in black satin finish. \

I say no to safari/trophy hunting for human pleasure!
