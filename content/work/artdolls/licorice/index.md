---
title: "Miss Heavenly Licorice"
date: "2013-03-05"
available: false
---

Skull doll

2013

H x W x D approx
- 35 x 26 x 15 cm
- 13.7 x 10.2 x 5.9 inch

Miss Heavenly Licorice her skull is an artificial resin kitten skull with realistic blue eyes and tiny lashes. \
Her hair is made from high quality black English viscose with a pink hairbow. \
Miss Heavenly Licorice her body is sculpted from paperclay over a metal frame, painted with acrylics, "tattooed" with watercolor pencils and then sealed with Mr. Clear. \
She is dressed in an antique black lace corset with attached neckpiece, lace knicker, girdle and a sinamay panniere adorned with powder pink silk ribbons, bows, 1mm golden beads and a netting petticoat. \
She is wearing black sheer stockings and lambs leather heelless boots. \
She is posed (permanent) on a black wooden base adorned with black miniature gravel and powder silk ribbon.
