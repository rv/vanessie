---
title: "Wintertime"
date: "2009-10-03"
available: false
---

Skull doll

19 cm / 7.5 inch

Bird skull, apoxie, leather, fake fur, viscose hair, fake snow, handmade porcelain collectible miniature Husky.
