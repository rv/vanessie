---
title: "An Orphan Called Darkness"
date: "2010-02-26"
available: false
---

Skull doll

2010

42 cm / 16.5 inch

Her head is partially a duck skull and partially apoxie sculpt with English viscose hair. \
The body with X-knees is a full sculpt body made of apoxie sculpt. \
The wool felted cape is embroided with many antique (late 1800) jet mourning beads and can be removed. \
The dress is made of silk and linen with antique mourning jet. \
Under her dress she wears undies from antique fabric and lace (late 1800). \
Her shoes and suitcase are from lambs leather and are handmade. \
The little voodoo doll is also made from the antique fabric.
