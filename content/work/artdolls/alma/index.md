---
title: "Alma and Nibbles"
date: "2010-09-08"
available: false
---

27 cm / 10.6 inch (would be approx. 43 cm / 16.9 inch when standing)

Alma with her pet Nibbles, in mourning for her parents. \
This doll is inspired by the <a href="http://www.youtube.com/watch?v=63fwk_pQFIE">short movie Alma</a>

Alma is made from paper clay (head and hands) and Apoxie sculpt (entire body) over a strong wire frame. \
Her hair is made from the softest black Tibetan lamb, and has a black/white checkered bow. \
The dress is made from black dupioni silk and black/white ribbons and have a white taft silk collar with a tiny cameo. \
Alma is wearing a black/white stripped legging, black camisole and a black bloomer (with ribbon bows) underneath her dress. \
Her Victorian child shoes are made from soft lambs leather. \
Alma is holding a picture (Victorian memorial photo printed on glossy photo paper) of her parents in her hand.

Nibbles is a real little mouse skeleton with a soft pink ribbon bow. \
7 cm / 2.8 inch long and 3 cm / 1.2 inch high.
