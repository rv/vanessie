---
title: "The Wind That Shakes The Cherry Tree"
date: "2013-05-13"
available: false
---

Skull doll

2013

H x W x D approx
- 40 x 23 x 22 cm
- 15.7 x 9 x 8.6 inch

This doll is a tribute to all who survived or lost the battle with breast cancer.

The skull is a sculpted paperclay skull with closed eyes, tiny lashes and a real Ibis beak. \
Her hair is made from high quality black English viscose balls with red chenille balls. \
The body is sculpted from paperclay over a metal frame, painted with acrylics and then sealed with Mr. Clear. \
She is wearing a warm red petite point silk kimono dress with a back train that is adorned with traditional embroidery and silk ribbon embroidery on the in- and outside. \
The sleeve and front panels are adorned with tiny black seed beads. \
The necklace is made of tiny fresh water pearls with one bigger focal fresh water pearl. \
Her feet are made as traditional Japanese foot bindings. \
She is posed on a black double wooden base with a gold metal name tag.
