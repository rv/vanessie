---
title: "La Pastorella"
date: "2010-12-01"
available: false
---

Automata skull doll

2010

- Doll 26 cm / 10.2 inch
- Total piece 40 cm / 15.7 inch

When turning the handle the sheep will jump. \
The automata base is made of wood, metal and decoreted with a sepia picture on the inside. \
The wood is treated with antique oil. \
Little lamb is sculpted from polymer clay finished with cotton fibers from the cotton plant. \
She is wearing a golden metal crown with a pearl inside and a silk distressed ribbon with tiny bells. \
La Pastorella is sculpted from apoxie and paperclay, her beautiful head is a Bearded Barbet skull with apoxie. \
Her eyes are synthetic and her hair is beautiful pure white Bombay Mulberry Silk. \
The dress is made from velvet with a leather corset, decorated with the tiniest wooden buttons, a leather belt bag, a scissor, and skulls. \
La Pastorella wears leather hunting boots with fat rubber soles. \
The roses crown Pastorella wears in her hair is made from Mulberry paper roses and silk distressed silk ribbon. \
<a href="http://vimeo.com/21263084" target="_blank">Video</a><
