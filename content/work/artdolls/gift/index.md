---
title: "The Gift"
date: "2012-11-25"
available: false
---

Skull doll

2012

H x W x D approx.
- 40 x 15 x 15 cm
- 15.7 x 5.9 x 5.9 inch

The Gift her skull is a paperclay parrot skull with handmade glass eyes and tiny lashes. \
Her hair is made from high quality black English viscose and she is wearing a fascinator from satin, fur, silk, and 2 of the tiniest real skulls (a rodent and a reptile). \
Her body is sculpted with paperclay over a metal frame. \
The Gift is dressed in black and bordeaux velvet (cotton/silk velvet), antique black lace, and silk ribbon adorned with an antique gold pin and tiny cameo. \
In her hand she is holding the gift: a real tiny dried lizard. \
In her other hand she is holding a leash with her real dried fish pets. \
She is posed (permanently) on a wooden base.
