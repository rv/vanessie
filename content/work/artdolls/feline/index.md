---
title: "Feline La Rouge"
date: "2010-10-11"
available: false
---

Skull doll

2010

59 cm / 23.2 inch

Feline's head is a real cat skull with high quality glass eyes and full, long eyelashes set in Apoxie sculpt. \
Her hair is copper red high quality English viscose in boogie woogie bugle boy style. \
Feline's body is made from a layer paper clay over apoxie on a strong wire skeleton, painted with acrylics. \
She's wearing a 100% dupioni silk wiggle dress with silk crepe georgette sleeves. \
She has breast and sleeve tattoos. \
Her high heeled shoes are made from soft deer leather with (faux) golden buggles. \
Feline holds a faux vintage fox in her hands (fake �designer� fur).
